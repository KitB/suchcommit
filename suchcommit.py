from config import USERNAME, PASSWORD
import requests
import json

# http://restbrowser.bitbucket.org/
# http://blog.bitbucket.org/2013/11/12/api-2-0-new-function-and-enhanced-usability/

# get repos and attributes
# get a repo commits
# visualise repo commits and attributes


def get_repos(owner=None, page=1, repos=None):

    owner = owner or USERNAME
    repos = repos or []
    done = False

    url = "https://bitbucket.org/api/2.0/repositories/%s" % USERNAME

    while not done:

        params = {"page": page}
        r = requests.get(url, auth=(USERNAME, PASSWORD), params=params)
        raw = json.loads(r.text)

        try:
            # Repos found.
            for data in raw["values"]:
                repo = {}
                repo["slug"] = data["full_name"][len(USERNAME) + 1:]
                repo["name"] = data["name"]
                repo["description"] = data["description"]
                repo["private"] = data["is_private"]
                repo["language"] = data["language"]
                repo["created"] = data["created_on"]
                repo["updated"] = data["updated_on"]
                repo["size"] = data["size"]

                repos.append(repo)

                print repo["slug"]

            page = page + 1
            print "Repos page %s" % page
        except KeyError:
            done = True

    return repos


def get_commits(slug, owner=None, page=1, commits=None):

    owner = owner or USERNAME
    commits = commits or []
    done = False

    url = "https://bitbucket.org/api/2.0/repositories/%s/%s/commits/" % (owner, slug)

    while not done:
        params = {"page": page}
        r = requests.get(url, auth=(USERNAME, PASSWORD), params=params)
        commits_list = json.loads(r.text)

        try:
            # There are some commits.
            # Extract the bits we care about.
            for commit in commits_list["values"]:
                commits.append({
                    "id": commit["hash"],
                    "date": commit["date"],
                    "text": commit["message"]
                })
            page = page + 1
            print "%s commits page %s" % (slug, page)
        except KeyError:
            # No commits left (paged out).
            done = True

    return commits


def get_everything(owner=None):

    owner = owner or USERNAME

    repos = get_repos()
    for repo in repos:
        repo["commits"] = get_commits(repo["slug"])

    return repos

if __name__ == '__main__':
    things = get_everything()
    print len(things)
